export class DateHelper{

        /**
     * 
     * @param date: string - data no formato iso. 
     */
         static breakDate(date){
            const aux = (date.split('T')[0]).split('-');
            return{ ano: parseInt(aux[0], 10), mes: parseInt(aux[1], 10), dia: parseInt(aux[2], 10)};
          }

}