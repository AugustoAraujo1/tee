// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyARG2rqLfjIFp_MqKhkS0QRrrDPDJHWb-8',
    authDomain: 'controle-ifsp-2021.firebaseapp.com',
    projectId: 'controle-ifsp-2021',
    storageBucket: 'controle-ifsp-2021.appspot.com',
    messagingSenderId: '22461954153',
    appId: '1:22461954153:web:c9e3c45117f0cf7743caf8',
    measurementId: 'G-TF99BM8W81'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
